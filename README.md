Expert Flooring Solutions is Las Vegas’ premier flooring and carpet store. We offer the highest quality brands of carpet, hardwood, laminate, and tile, as well as a variety of vinyl plank and waterproof flooring options.

Address: 6485 S Rainbow Blvd, #100, Las Vegas, NV 89118, USA

Phone: 702-524-4940

Website: https://expertflooring.net

